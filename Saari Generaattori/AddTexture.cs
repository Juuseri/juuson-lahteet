﻿using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Oma luokka käsittelemään korkeuskartan arvoja. Säikeestä ei voi käyttää Unityn APIa
/// joten esimerkiksi terrainData.GetSteepness(x,y) ei toimi sieltä. Joten tallennetaan tarvittavat
/// korkeuskartan tiedot omaan olioon, jolloin säiekin voi niitä käyttää
/// </summary>
public class CustomHeightMap
{
    //jyrkkyys taulukko
    public float[,] steepness;
    //korkeustaulukko
    public float[,] height;

    /// <summary>
    /// Luokan konstruktori
    /// </summary>
    /// <param name="mapWidth">Kartan leveys</param>
    /// <param name="mapHeight">Kartan korkeus</param>
    public CustomHeightMap(int mapWidth, int mapHeight)
    {
        steepness = new float[mapWidth, mapHeight];

        height = new float[mapWidth, mapHeight];
    }

}

public class AddTexture : MonoBehaviour {

    private CustomHeightMap customHeightMap;

    Thread th1;
 
    //kartan arvojen muuttujia
    private Terrain terrain;
    private TerrainData terrainData;
    int alphaWidth;
    int alphaHeight;
    int alphaLayers;
    int mapHeight;
    int mapWidth;

    //splatmap, johon tekstuurien painotukset säilötään
    private float[,,] splatmapData;

    //seuraavat muuttujat ovat käyttäjän syöttämiä tekstuureja kartalle ja
    //arvoja milloin ne esiintyvä (millä korkeuksilla ja kaltevuuksilla mikäkin esiintyy)
    public Texture2D pohjaTex;
    public Texture2D pohjaNormal;

    [Range(0f, 600f)]
    public float vesiraja = 0f;
    public Texture2D hiekkaTex;
    public Texture2D hiekkaNormal;

    [Range(0f, 600f)]
    public float vuoriraja = 0f;
    public Texture2D vuoriTex;
    public Texture2D vuoriNormal;

    public Texture2D jyrkkaTex;
    public Texture2D jyrkkaNormal;
    [Range(0f, 90f)]
    public float kallioKulma = 40f;
    [Range(0f, 90f)]
    public float kallioKulmanSekoitusVali = 5.0f;
    [Range(1f, 10.0f)]
    public float kallioKulmanSekoitusIntensity = 1.0f;

    public Texture2D loivaTex;
    public Texture2D loivaNormal;
    [Range(0f, 90f)]
    public float ruohoKulma = 20f;
    [Range(0f, 90.0f)]
    public float kulmanSekoitusVali = 5.0f;
    [Range(1f, 10.0f)]
    public float kulmanSekoitusIntensity = 1.0f;

    [Range(0f, 10.0f)]
    public float hiekkaSekoitusVali = 2f;

    [Range(0f, 10.0f)]
    public float vuoriSekoitusVali = 2f;

    private bool threadPaalla = true;
    private bool tehty;

    /// <summary>
    /// funktio säikeelle
    /// </summary>
    void Thread1()
    {
        //säie käyntiin
        while (threadPaalla)
        {
            //heti kun edellinen teksturointi on tehty kartalle, säie saa tehdä laskun uudestaan ja
            //kun lasku on tehty, emme laske enää uutta teksturointia, ennenkuin vanha on lisätty kartalle
            if (!tehty)
            {
                Texturize();
                tehty = true;
            }
        }
    }

    /// <summary>
    /// kun "peli" suljetaan, sammutetaan myös säie, jottei se jää päälle ja Unity hermostu
    /// </summary>
    void OnApplicationQuit()
    {
        threadPaalla = false;
        th1.Abort();
        Debug.Log("Application ending after " + Time.time + " seconds");
    }

    // Use this for initialization
    // alustetaan tässä kaikki tarvittavat arvot
    void Start()
    {
        tehty = false;
        float kulma = 30f;
        float tamaKulma = 40f;
        float vali = 10f;

        float erotus = (kulma + vali) - (kulma - vali);
        float muutettuTamakulma = tamaKulma - (kulma - vali);
        float test = 1f - (muutettuTamakulma / erotus);
        print(erotus);
        print(muutettuTamakulma);

        print(test);
        print(Mathf.Clamp01(test));

        //otetaan objektin terrain-komponentti ja siitä kaikki tarvittavat tiedot
        terrain = GetComponent<Terrain>();

        terrainData = terrain.terrainData;

        alphaWidth = terrainData.alphamapWidth;
        alphaHeight = terrainData.alphamapHeight;
        alphaLayers = terrainData.alphamapLayers;

        mapHeight = terrainData.heightmapHeight;
        mapWidth = terrainData.heightmapWidth;

        //asetetaan uuteen SplatPrototypeen (taulukko johon kartan tekstuurit säilötään)
        //ja asetetaan käyttäjän asettamat tekstuurit sinne, jos niitä on
        SplatPrototype[] terrainTexture = new SplatPrototype[5];
        terrainTexture[0] = new SplatPrototype();
        if (pohjaTex != null) terrainTexture[0].texture = pohjaTex;
        terrainTexture[0].tileSize = new Vector2(5f, 5f);
        if (pohjaNormal != null) terrainTexture[0].normalMap = pohjaNormal;

        terrainTexture[1] = new SplatPrototype();
        if (hiekkaTex != null)terrainTexture[1].texture = hiekkaTex;
        terrainTexture[1].tileSize = new Vector2(15f, 15f);
        if (hiekkaNormal != null)terrainTexture[1].normalMap = hiekkaNormal;

        terrainTexture[2] = new SplatPrototype();
        if (vuoriTex != null)terrainTexture[2].texture = vuoriTex;
        terrainTexture[2].tileSize = new Vector2(5f, 5f);
        if (vuoriNormal != null)terrainTexture[2].normalMap = vuoriNormal;

        terrainTexture[3] = new SplatPrototype();
        if (jyrkkaTex != null)terrainTexture[3].texture = jyrkkaTex;
        terrainTexture[3].tileSize = new Vector2(10f, 10f);
        if (jyrkkaNormal != null)terrainTexture[3].normalMap = jyrkkaNormal;

        terrainTexture[4] = new SplatPrototype();
        if (loivaTex != null)terrainTexture[4].texture = loivaTex;
        terrainTexture[4].tileSize = new Vector2(1.5f, 1.5f);
        if (loivaNormal != null) terrainTexture[4].normalMap = loivaNormal;

        terrainData.splatPrototypes = terrainTexture;

        //alustetaan oma korkeuskartta oliomme säiettä varten
        customHeightMap = new CustomHeightMap(alphaWidth, alphaHeight);

        for (int y = 0; y < alphaHeight; y++)
        {
            for (int x = 0; x < alphaWidth; x++)
            {
                // Normalise x/y coordinates to range 0-1 
                float y_01 = (float)y / (float)alphaHeight;
                float x_01 = (float)x / (float)alphaWidth;

                // Sample the height at this location (note GetHeight expects int coordinates corresponding to locations in the heightmap array)
                customHeightMap.height[y, x] = terrainData.GetHeight(Mathf.RoundToInt(y_01 * mapHeight), Mathf.RoundToInt(x_01 * mapWidth));

                // Calculate the steepness of the terrain
                customHeightMap.steepness[y, x] = terrainData.GetSteepness(y_01, x_01);
            }
        }

        //ajetaan alustava teksturointi läpi
        Texturize();

        //laitetaan säie päälle, joka hoitaa Texturize() funktion ajon tästä lähtien
        th1 = new Thread(new ThreadStart(Thread1));
        th1.Start();

    }

    // Update is called once per frame
    void Update()
    {
        //kun säie kertoo, että on laskenut teksturoinnin, voimme lisätä splatmapin karttaan
        if (tehty)
        {
            terrainData.SetAlphamaps(0, 0, splatmapData);
            tehty = false;
        }

    }

    /// <summary>
    /// funktio kartan teksturointiin
    /// </summary>
    void Texturize()
    {
        //uusi tyhjä splatmappi tekstuurien painotuksia varten
        splatmapData = new float[alphaWidth, alphaHeight, alphaLayers];

        //väliaikainen tutoriaalista katsottu ja kopioitu, mutta hiukan omaan makuun muokattu splatmapin käsittely,
        //kun vielä splatmapin toiminta on itselle hiukan hämärän peitossa
        for (int y = 0; y < alphaHeight; y++)
        {
            for (int x = 0; x < alphaWidth; x++)
            {
                // Normalise x/y coordinates to range 0-1 
                float y_01 = (float)y / (float)alphaHeight;
                float x_01 = (float)x / (float)alphaWidth;

                // Sample the height at this location (note GetHeight expects int coordinates corresponding to locations in the heightmap array)
                float height = customHeightMap.height[y, x]; //terrainData.GetHeight(Mathf.RoundToInt(y_01 * mapHeight), Mathf.RoundToInt(x_01 * mapWidth));

                // Calculate the normal of the terrain (note this is in normalised coordinates relative to the overall terrain dimensions)
                //Vector3 normal = terrainData.GetInterpolatedNormal(y_01, x_01);

                // Calculate the steepness of the terrain
                float steepness = customHeightMap.steepness[y, x]; //terrainData.GetSteepness(y_01, x_01);

                // Setup an array to record the mix of texture weights at this point
                float[] splatWeights = new float[alphaLayers];

                // CHANGE THE RULES BELOW TO SET THE WEIGHTS OF EACH TEXTURE ON WHATEVER RULES YOU WANT

                // ensimmäinen tekstuuri (nurmi/multa) on pohjavärinä
                splatWeights[0] = 1.0f;


                float erotus = hiekkaSekoitusVali * 2f;
                float muutettuTamakulma = height - (vesiraja - hiekkaSekoitusVali);
                float test = 1f - (muutettuTamakulma / erotus);

                //jos korkeus alkaa mennä vesirajan alapuolelle, alamme värjäämään sitä hiekan tekstuurilla
                if (height <= vesiraja + hiekkaSekoitusVali) splatWeights[1] = 1.0f * Mathf.Clamp01(test);
                if (height <= vesiraja - hiekkaSekoitusVali) splatWeights[1] = 2.0f;

                erotus = hiekkaSekoitusVali * 2f;
                muutettuTamakulma = height - (vuoriraja - hiekkaSekoitusVali);
                test = (muutettuTamakulma / erotus);

                //jos korkeus alkaa taas mennä vuorirajan yläpuolelle, alamme värjäämään sitä vuoren tekstuurilla
                if (height >= vuoriraja - hiekkaSekoitusVali) splatWeights[2] = 1.0f * Mathf.Clamp01(test);
                if (height >= vuoriraja + hiekkaSekoitusVali) splatWeights[2] = 2.0f;



                float u = 0;

                for (int i = 0; i < splatWeights.Length; i++)
                {
                    u += splatWeights[i];
                }

                // Loop through each terrain texture
                for (int i = 0; i < alphaLayers; i++)
                {

                    // Normalize so that sum of all texture weights = 1
                    splatWeights[i] /= u;

                    // Assign this point to the splatmap array
                    splatmapData[x, y, i] = splatWeights[i];
                }


                //tässä vaiheessa pohjatekstuurit (hiekka->multa/heinä->kallio) ovat aseteltu korkeuden mukaan
                //ja seuraavaksi lisäämme hiukan yksityiskohtaisuutta lisäämällä vehreämpää ruohoa tasaisille alueille ja
                //paljasta kalliota tarpeeksi jyrkille alueille

                erotus = kallioKulmanSekoitusVali*2f;
                muutettuTamakulma = steepness - (kallioKulma - kallioKulmanSekoitusVali);
                test = (muutettuTamakulma / erotus);

                if (steepness >= kallioKulma - kallioKulmanSekoitusVali) splatWeights[3] = 1.0f * Mathf.Clamp01(test);
                if (steepness >= kallioKulma) splatWeights[3] = 1.0f * kallioKulmanSekoitusIntensity;

                erotus = kulmanSekoitusVali*2f;
                muutettuTamakulma = steepness - (ruohoKulma - kulmanSekoitusVali);
                test = 1f - (muutettuTamakulma / erotus);

                if (steepness <= ruohoKulma + kulmanSekoitusVali && height < vuoriraja-vuoriSekoitusVali && height > vesiraja+ hiekkaSekoitusVali) splatWeights[4] = 1.0f * Mathf.Clamp01(test);

                erotus = hiekkaSekoitusVali * 2f;
                muutettuTamakulma = height - (vesiraja - hiekkaSekoitusVali);
                test = (muutettuTamakulma / erotus);

                if (steepness <= ruohoKulma + kulmanSekoitusVali && height < vesiraja + hiekkaSekoitusVali && height > vesiraja) splatWeights[4] = 1.0f * Mathf.Clamp01(test);
                
                erotus = vuoriSekoitusVali * 2f;
                muutettuTamakulma = height - (vuoriraja - vuoriSekoitusVali);
                test = 1f - (muutettuTamakulma / erotus);

                if (steepness <= ruohoKulma + kulmanSekoitusVali && height > vuoriraja - vuoriSekoitusVali && height < vuoriraja) splatWeights[4] = 1.0f * Mathf.Clamp01(test);
                
                if (steepness <= ruohoKulma && height < vuoriraja- vuoriSekoitusVali && height > vesiraja + vuoriSekoitusVali) splatWeights[4] = 1.0f * kulmanSekoitusIntensity;

                float z = 0;

                for (int i = 0; i < splatWeights.Length; i++)
                {
                    z += splatWeights[i];
                }

                // Loop through each terrain texture
                for (int i = 0; i < alphaLayers; i++)
                {

                    // Normalize so that sum of all texture weights = 1
                    splatWeights[i] /= z;

                    // Assign this point to the splatmap array
                    splatmapData[x, y, i] = splatWeights[i];
                }
            }
        }
    }
}

