﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class terraingenerate : MonoBehaviour {

    //objektin terrain komponentti
    private Terrain terrain;

    //kaikki seuraavat publicit ovat säädettävissä olevia arvoja editorissa, jotta käyttäjä
    //saa pinnan muodot haluamansa tyylisiksi

    //kuinka monta iteraatiota PerTurb funktiota tehdään
    public int iterations = 7;

    //tällä hetkellä käyttämätön arvo iteraatioiden suorituksen katkaisemiseen, jolloin siinä välissä
    //pystyi ajamaan muita algoritmeja pinnan muodolle, mutta se osoittautui hiukan tarpeettomaksi.
    public int iterationSplit = 0;

    //kuinka monesti tehtyä maanmuotoa halutaan alentaa/tasoittaa
    public int seaFloorIter = 1;

    //perlin noiselle tarpeelliset funktiot ("aallon" amplitudi ja taajuus)
    public float origPerlinHeight = 0.15f;
    public float origPerlinScale = 2.0f;

    //käytetäänkö sattumanvaraista seediä vai käyttäjän itse valitsemaa
    public bool UseRandom = false;

    //sen hetkinen randomgeneraattorin seed
    public string seed = "0";

    //numeroarvo seedille
    public float rnd = 0f;

    //muuttuvat perlin noisen arvot, koska turbulenssin saavuttamiseksi, tarvitsemme uusille iteraatioille
    //pienempiä arvoja alkuperäisistä arvoista
    float perlinHeight;
    float perlinScale;

    //kartan korkeus ja pituus
    int mapHeight;
    int mapWidth;

    //käytettäviä muuttujia kartan korkeustaulukosta
    private float[,] tmpTerrain;
    private float[,] originalTerrainData;
    private float[,] thisTerrain;

    //säie ja muuttujia sen käyttöön
    public Thread th1;
    private bool isDone = false;
    private bool threadPaalla = true;
    private bool meneThread = false;

    //laskuri näyttämään minulle hiukan, kuinka monta kierrosta algoritmit vetävät.
    //Tulevaisuudessa pystyn näitä arvoja käyttämään näkyvässä progress barissa,
    //jotta käyttäjä tietää missä vaiheessa laskut ovat menossa
    public int totalLoops;
    public int calculatedLoops = 0;

    public string status = "";

    /// <summary>
    /// funktio säikeelle
    /// </summary>
    void Thread1()
    {
        while (threadPaalla)
        {
            //jos saamme käskyn lähteä laskemaan pinnan muotoja, se alkaa tästä ja
            //meneThread on true
            if (meneThread)
            {
                //alussa hiukan lasken kuinka paljon kunkinhetkisen kartan
                //tarvitsee loopata
                calculatedLoops = 0;

                int f = 0;
                print("Threadissa");
                for (int i = 0; i < mapWidth; i++)
                {
                    for (int k = 0; k < mapHeight; k++)
                    {
                        tmpTerrain[i, k] = 0;
                        f++;
                        calculatedLoops++;
                    }
                }

                print("Alustus Tehty, "+f+" solua käyty");

                //ensimmäinen iteraatio perlin noise turbulenssista
                Tee();

                print("Eka Tee tehty");

                //seuraavaksi käymme loput iteraatiot
                for (int i = 0; i < iterations; i++)
                {
                    TeeToinen();
                }

                print("Toka Tee tehty");

                //lopuksi olisi maaston tasoitus/alentaminen, mutta sillä ei tällähetkellä tuntunut
                //olevan kauheasti vaikutusta lopputulokseen, joten se on vielä otettu pois
                for (int i = 0; i < seaFloorIter; i++)
                {
                    //ResetSeaFloor();
                }

                print("Lattia Tasattu");

                //jos käyttäisimme iteraatioiden katkaisua, tämä tulisi seuraavaksi, mutta silläkään
                //ei tuntunut olevan suurta vaikutusta lopputulokseen, joten sekin on vielä piilossa
                for (int i = 0; i < iterationSplit; i++)
                {
                    //TeeToinen();
                }

                print("Toinen Tee");

                //lopuksi lisäämme tehdyn terrainin alkuperäisen saarialgoritmin tekemään saaripohjaan (tai "maskiin")
                //jolloin perturbin tekemät maanmuodot vaikuttavat vain ja ainoastaan niissä kohdin missä on saarta
                thisTerrain = new float[mapWidth, mapHeight];
                for (int i = 0; i < mapWidth; i++)
                {
                    for (int k = 0; k < mapHeight; k++)
                    {
                        thisTerrain[i, k] = originalTerrainData[i, k] * tmpTerrain[i, k];
                        calculatedLoops++;
                    }
                }

                print("Fusetettu");

                //korkeusarvot on laskettu, joten muu koodi voi lisätä korkeudet itse terrainiin, jotta näemme minkälainen maasto tuli.
                //korkeusarvojen lisäämistä ei voida tehdä säikeessä, siksi joudun käyttämään vain booleania joka ilmoittaa muulle koodille,
                //milloin se on valmis laskuissaan
                isDone = true;
                meneThread = false;
            }
            Thread.Sleep(100);
        }
    }

    // Use this for initialization
    // alustetaan kaikki tarvittavat arvot kartasta, kuten sen korkeudet, leveydet ja pituudet,
    void Start () {

        isDone = false;
        th1 = new Thread(new ThreadStart(Thread1));
        th1.Start();

        terrain = GetComponent<Terrain>();

        totalLoops = ((terrain.terrainData.heightmapHeight* terrain.terrainData.heightmapHeight) * (iterations + 3));

        mapHeight = terrain.terrainData.heightmapHeight;
        mapWidth = terrain.terrainData.heightmapWidth;

        thissTerrain = new float[mapWidth, mapHeight];

        tmpTerrain = new float[mapWidth, mapHeight];
        originalTerrainData = terrain.terrainData.GetHeights(0, 0, mapWidth, mapHeight);

        for (int i = 0; i < mapWidth; i++)
        {
            for (int k = 0; k < mapHeight; k++)
            {
                tmpTerrain[i, k] = 0;
            }
        }
        
    }

    /// <summary>
    /// Kun "peli" loppuu suljemme säikeen, muuten Unity menee jumiin jos siltä jää säie päälle vaikka peli ei
    /// enää pyöri ja yritämme käynnistää koko homman uudestaan.
    /// </summary>
    void OnApplicationQuit()
    {
        th1.Abort();
        threadPaalla = false;
        Debug.Log("Application ending after " + Time.time + " seconds");
    }

    // Update is called once per frame
    void Update () {


        status = calculatedLoops.ToString() + "/" + totalLoops.ToString();
        if (Input.GetKeyDown(KeyCode.E)) PrintHeight();
        if (Input.GetKeyDown(KeyCode.W)) Circle();
        if (Input.GetKeyDown(KeyCode.X)) Revert();
        if (Input.GetKeyDown(KeyCode.F)) th1.Abort();
        if (Input.GetKeyDown(KeyCode.Space)) TeeTerrain();
        if (Input.GetKeyDown(KeyCode.A)) Flatten();
        if (Input.GetKeyDown(KeyCode.S)) TeeKolmas();
        if (Input.GetKeyDown(KeyCode.Q)) BlendHeights();

        //tällähetkellä tärkein nappi. Muut ovat olleet mukana vain testaillessa miltä eri algoritmit
        //näyttävät kartalla
        if (Input.GetKeyDown(KeyCode.D))
        {
            meneThread = true;
        }

        //kun saamme säikeeltä luvan, lisäämme korkeuskartan terrainiin
        if (isDone)
        {
            terrain.terrainData.SetHeights(0, 0, thisTerrain);
            isDone = false;
            //th1.Abort();
        }

    }

    ////////////////////////////////////////////////////////////////////////////////
    //Seuraavista funktioista vain Tee() ja TeeToinen() on käytössä, muut ovat
    //olleet alunperin käytössä vain testatakseni, miten terrain käyttäytyy
    //ja miten sitä voin käyttää. Ne ovat vielä varalta tallessa, mutta lähtevät
    //lopullisesta buildista pois.
    ////////////////////////////////////////////////////////////////////////////////

    void PrintHeight()
    {
        print(terrain.terrainData.GetHeight(512,512));
    }
    public float maskThresh = 0.2f;

    void Circle()
    {
        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
        sw.Start();

        //maskThresh = 0.1f;
        int center = mapWidth / 2;

        int x = UnityEngine.Random.Range(0, mapWidth);
        int y = UnityEngine.Random.Range(0, mapHeight);
        
        print("X: " + x + ", Y: " + y);
        int muutettuX = (x > mapWidth / 2) ? mapWidth - x : x;
        int muutettuY = (y > mapHeight / 2) ? mapHeight - y : y;

        int shortestToEdge = (muutettuX < muutettuY) ? muutettuX : muutettuY;

        print("muutettu X: "+ muutettuX + ", muutettu Y: "+ muutettuY);
        print("Lyhin: "+shortestToEdge);
        
        float relation =shortestToEdge / (mapWidth / 2f);
        float relation2 = (mapWidth / 2f) / shortestToEdge;
        
        for (int i = 0; i < shortestToEdge*2; i++)
        {
            for (int k = 0; k < shortestToEdge*2; k++)
            {
                float distance = Mathf.Sqrt(Mathf.Pow((shortestToEdge - i), 2) + Mathf.Pow((shortestToEdge - k), 2)); //Vector2.Distance(new Vector2(x, y), new Vector2(i, k));

                float maskPixel = Mathf.Clamp(((0.499f * 1f - (distance / (shortestToEdge*2))) * maskThresh * 1f), 0f, 600f);
                //print("lol");
                if (thissTerrain[(x - shortestToEdge) + i, (y - shortestToEdge) + k] < 0.1f && thissTerrain[(x - shortestToEdge) + i, (y - shortestToEdge) + k] <= maskPixel)
                    thissTerrain[(x - shortestToEdge) + i, (y - shortestToEdge) + k] = maskPixel;
            }
        }
        
        //float[,] thisTerrain = new float[mapWidth, mapHeight];
        /*
        for (int i = 0; i < mapWidth; i++)
        {
            for (int k = 0; k < mapHeight; k++)
            {
                float distance = Mathf.Sqrt(Mathf.Pow((x - i), 2) + Mathf.Pow((y - k), 2)); //Vector2.Distance(new Vector2(x, y), new Vector2(i, k));

                float maskPixel = Mathf.Clamp(((0.499f * relation - (distance / mapWidth)) * maskThresh * relation2), 0f, 600f);
                //print("lol");
                if(thissTerrain[i,k] < 0.1f && thissTerrain[i, k] <= maskPixel) thissTerrain[i, k] = maskPixel;
            }
        }
        */

        terrain.terrainData.SetHeights(0, 0, thissTerrain);

        sw.Stop();
        print(sw.Elapsed);
    }

    float[,] thissTerrain;

    void Revert()
    {

        float[,] thisTerrain = new float[mapWidth, mapHeight];

        for (int i = 0; i < mapWidth; i++)
        {
            for (int k = 0; k < mapHeight; k++)
            {
                thisTerrain[i, k] = originalTerrainData[i, k];
            }
        }

        terrain.terrainData.SetHeights(0, 0, thisTerrain);
    }

    void TeeTerrain()
    {
        print("terraini!");

        Flatten();

        TeeKolmas();

        terrain.terrainData.SetHeights(0, 0, tmpTerrain);
    }

    void Fuse()
    {
        /*
        for (int i = 0; i < mapWidth; i++)
        {
            for (int k = 0; k < mapHeight; k++)
            {
                tmpTerrain[i, k] = 0;
            }
        }

        TeeKolmas();
        */

        print("fuse");
        float[,] thisTerrain = new float[mapWidth, mapHeight];


        for (int i = 0; i < mapWidth; i++)
        {
            for (int k = 0; k < mapHeight; k++)
            {
                thisTerrain[i, k] = originalTerrainData[i, k] * tmpTerrain[i,k];
            }
        }

        terrain.terrainData.SetHeights(0, 0, thisTerrain);

    }

    void TeeKolmas()
    {
        //Flatten();

        Tee();

        for (int i = 0; i < iterations; i++)
        {
            TeeToinen();
        }

        for (int i = 0; i < seaFloorIter; i++)
        {
            ResetSeaFloor();
        }

        for (int i = 0; i < iterationSplit; i++)
        {
            TeeToinen();
        }

        //Tee();
    }

    /// <summary>
    /// perlin noise turbulenssin toinen vaihe, jossa alkuperäiseen "isoon" perlin noiseen
    /// aletaan lisätä pienempiä ja yksityiskohtaisempia muotoja
    /// </summary>
    void TeeToinen()
    {

        print("Iteraatio!");

        //joka iteraation alussa puolitamme "aallon" korkeuden mutta tihennämme taajuutta
        perlinHeight /= 2.0f;
        perlinScale *= 2.0f;

        /*
        float[,] heights = new float[terrain.terrainData.heightmapWidth, terrain.terrainData.heightmapHeight];

        for (int i = 0; i < terrain.terrainData.heightmapWidth; i++)
        {
            for (int k = 0; k < terrain.terrainData.heightmapHeight; k++)
            {
                heights[i, k] = 0;
            }
        }
        
        terrain.terrainData.SetHeights(0, 0, heights);
        */
        //int cHeight = terrain.terrainData.heightmapWidth;
        //int cWidth = terrain.terrainData.heightmapHeight;

        //float[,] originalMap = terrain.terrainData.GetHeights(0, 0, cWidth, cHeight);

        //float rnd = UseRandom ? (float)(DateTime.Now.Millisecond) / 1000f : 0f;

        int f = 0;
        //loopataan korkeustaulukko läpi ja teemme sen arvoille PerlinNoise() funktion.
        //looppaus ei siis korvaa alkuperäisiä arvoja vaan lisää siihen uudet yksityiskohdat
        for (int x = 0; x < mapWidth; x++)
        {
            for (int y = 0; y < mapHeight; y++)
            {

                float px = (float)x / (float)mapWidth;
                float py = (float)y / (float)mapHeight;

                tmpTerrain[x, y] += perlinHeight * Mathf.PerlinNoise((px + rnd) * perlinScale, (py + rnd) * perlinScale);
                f++;
                calculatedLoops++;
            }
        }

        print("Toka Tee Tehty, " + f + " solua käyty");

        //terrain.terrainData.SetHeights(0, 0, originalMap);
    }

    /// <summary>
    /// ensimmäinen iteraatio perlin noise turbulenssista. Täällä määräämme mm. randomgeneraattorin seedin
    /// ja pohjustamme korkeustaulukon ensimmäisellä, tavallisella perlin noisella, johon sitten alamme lisätä
    /// yksityiskohtia uusilla iteraatioilla
    /// </summary>
    void Tee()
    {
        perlinHeight = origPerlinHeight;
        perlinScale = origPerlinScale;

        //int cHeight = terrain.terrainData.heightmapWidth;
        //int cWidth = terrain.terrainData.heightmapHeight;
        //float[,] originalMap = terrain.terrainData.GetHeights(0, 0, cWidth, cHeight);
        //seed = DateTime.Now.GetHashCode().ToString();

        //jos käytämme sattumanvaraista seediä, otamme sen tämän hetkisen ajan HashCodesta, jotta
        //se on lähes varmasti erilainen joka kerta. Muutoin käytämme käyttäjän syöttämää seediä.
        seed = UseRandom ? DateTime.Now.GetHashCode().ToString(): seed;

        //muutamme seedin floatiksi ja jaamme sen hiukan pienemmäksi, jotta se kelpaa PerlinNoise():lle
        rnd = (float)(seed.GetHashCode()) / 10000000f;

        int f = 0;

        //loopataan korkeustaulukko läpi ja teemme sen arvoille ensimmäisen PerlinNoise() funktion,
        //joka toimii pohjana maastolle
        for (int x = 0; x < mapWidth; x++)
        {
            for (int y = 0; y < mapHeight; y++)
            {

                float px = (float)x / (float)mapWidth;
                float py = (float)y / (float)mapHeight;

                tmpTerrain[x, y] += perlinHeight * Mathf.PerlinNoise((px + rnd) * perlinScale, (py + rnd) * perlinScale);

                f++;
                calculatedLoops++;
            }
        }

        print("Eka Tee Tehty, " + f + " solua käyty");

        //terrain.terrainData.SetHeights(0, 0, originalMap);
    }

    public void ResetSeaFloor()
    {
        //terrain = GetComponent<Terrain>();
        //float[,] heights2 = terrain.terrainData.GetHeights(0, 0, terrain.terrainData.heightmapWidth, terrain.terrainData.heightmapWidth);
        bool hitFloor = false;

        //Even if we hit 0 at some point, the function runs until the end of the map,
        //so there wont be any height differences. E.G. if the lowest point is at the
        //center of the map, it doesn't stop there but goes through the map so every
        //point is equally lowered.
        while (!hitFloor)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                for (int y = 0; y < mapHeight; y++)
                {
                    //calculatedLoops++;
                    tmpTerrain[x, y] -= 0.001f;
                    if (tmpTerrain[x, y] <= 0)
                        hitFloor = true;
                }
            }
        }

        //terrain.terrainData.SetHeights(0, 0, heights2);
    }

    public void BlendHeights()
    {

        //int hMapWidth = terrain.terrainData.heightmapWidth;
        //int hMapHeight = terrain.terrainData.heightmapHeight;

        float[,] heights2 = terrain.terrainData.GetHeights(0, 0, mapWidth, mapHeight);

        for (int x = 0; x < mapWidth; x++)
        {
            for (int y = 0; y < mapHeight; y++)
            {
                float pointHeight = 0.0f;
                float blendHeight = pointHeight;
                float pixelCount = 0.0f;

                for (int x2 = x - 1; x2 < x + 2; x2++)
                {
                    for (int y2 = y - 1; y2 < y + 2; y2++)
                    {
                        if (x2 >= 0 && y2 >= 0 && x2 < mapWidth && y2 < mapHeight)
                        {
                            blendHeight = blendHeight + heights2[x2, y2];
                            pixelCount++;
                        }
                    }
                }
                blendHeight = blendHeight / pixelCount;
                //print ("point: "+pointHeight+", blend: "+blendHeight);
                //if(blendHeight < 0.1f) heights2[x,y] = blendHeight;
                heights2[x, y] = blendHeight;
            }
        }
        terrain.terrainData.SetHeights(0, 0, heights2);
    }

    void Flatten()
    {

        float[,] heights = new float[mapWidth, mapHeight];

        for (int i = 0; i < mapWidth; i++)
        {
            for (int k = 0; k < mapHeight; k++)
            {
                heights[i, k] = 0;
                tmpTerrain[i, k] = 0;
            }
        }

        terrain.terrainData.SetHeights(0, 0, heights);
    }
}
