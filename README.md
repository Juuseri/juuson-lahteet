### Noppa kansiossa ###
* Nopan heiton skripti.
### Saari Generaattori kansiossa ###
* Alkuperäinen IslandGenerator.cs, joka on julkaistu versio Unityn kauppapaikalla. Siihen on vain lisätty funktio Test(x,y), jossa on uusi tyyli rantojen tekoon alkuperäisen RandomDrop(x,y) tilalle.
* Keskeneräiset AddTexture.cs ja terraingenerate.cs, joista terraingenerate.cs lisää saaripohjien päälle pinnanmuotoja Perlin Noise Turbulenssia käyttäen ja AddTexture lisää automaattisesti valmista
tekstuuripohjaa pinnan muotojen mukaisesti.