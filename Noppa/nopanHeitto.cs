﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
using System.Collections.Generic;

public class nopanHeitto : MonoBehaviour
{
    //nopan nopeus
    private float speed;
    //muuttuja arvioimaan onko noppa liikkeessä
    private bool liikkeessa;
    //muuttuja kertomaan onko noppa pysähtynyt (eli onko noppa ollut liikkumatta tarpeeksi kauan)
    public bool pysahtyi;
    //muuttuja kertomaan, koskettaako noppa pelilaudanpohjaa
    public bool onkoPohja;
    //aikalaskuri
    private float time;
    //nopan pyörimisnopeus heittäessä
    private float angularVelo;

    //muuttuja nopalle ilmaisemaan onko noppa jäädytetty paikalleen, vai vapaa liikkumaan
    public bool freezattu;
    //muuttuja nopalle ilmaisemaan kohdistuuko siihen yksittäinen heitto
    public bool singleHeitto;
    public bool heittoFreezenJalkeen;

    //kuluuko nopista ääntä (asetuksista säädettävä 1=nopista tulee ääni, 0=ei ääntä
    private int throwTila;

    private float ajastin;
    private bool saaHeittaa;

    //oman nopan nimi
    private GameObject nopanNimi;
    //nopan äänilähde
    private AudioSource sound;
    
    //nopan silmälukulaskuri-objekti
    private GameObject silmalasku;

    //listat erilaisista heittoäänistä
    private Object[] shorts;
    private Object[] longs;

    //kiihdytysmittarin mittaama nopeus puhelimesta (tätä käytetään minimiarvona, jolloin noppaa heitetään)
    private float acc;

    //suurimmat koordinaatit, jotta noppa ei lennä pelilaudan yli
    //(näkymättömät seinät eivät kelvanneet, sillä joskus Unityn fysiikkalasku päästi nopat seinien läpi,
    //joten noppia piti rajoittaa muulla tapaa. Tämä on todella karkea tapa toteuttaa tälläinen, nykyään
    //keksisin paremman, esimerkiksi Unityn oma Mathf.Clamp rajoittaa jonkun arvon min ja max arvojen sisään...
    public float posX, posZ;

    //tämän nopan silmäluku
    public int silmaluku;

    //käytetäänkö noppien heittoon gyroskooppia/kiihdytysmittaria
    private int acceToggle;

    public GameObject ascpectCheck;
    
    // Use this for initialization
    //alustetaan kaikki tarvittavat asiat nopalle
    void Start()
    {
        //noppa ilmestyessään on aina liikkeessä joten tämän voi alustaa trueksi
        liikkeessa = true;
        //etsitään objekti joka näyttää silmäluvut
        silmalasku = GameObject.FindGameObjectWithTag("pisteet");

        //haetaan asetuksista, mitä heittotilaa käytetään
        throwTila = PlayerPrefs.GetInt("throw", 1);

        //haetaan nopan oma äänilähde
        sound = GetComponent<AudioSource>();

        //jos kyseessä on kolikko, haetaan kolikkoäänet. Muutoin haetaan noppaäänet
        if (Regex.IsMatch(GetComponent<MeshFilter>().mesh.name, "^Coin"))
        {
            shorts = Resources.LoadAll("rolls/coin/short/");
        }
        else
        {
            shorts = Resources.LoadAll("rolls/short/");
            longs = Resources.LoadAll("rolls/long/");
        }

        //alustetaan kaikki perusarvot
        freezattu = false;
        singleHeitto = false;
        heittoFreezenJalkeen = true;
        pysahtyi = false;
        onkoPohja = false;
        saaHeittaa = true;

        silmaluku = 0;
        time = 0f;
        angularVelo = 50.0f;

        ascpectCheck = GameObject.FindGameObjectWithTag("MainCamera");
        posX = ascpectCheck.GetComponent<AspectRatioCheck>().xSeina;
        posZ = 9.5f;
        acc = 0.7f;

        //käytetäänkö puhelimen gyroa vai ei, haetaan arvo pelaajan tallentamista asetuksista
        acceToggle = PlayerPrefs.GetInt("acce", 1);
        if (acceToggle == 1)
        {
            Input.gyro.enabled = true;
        }
        else
        {
            Input.gyro.enabled = false;
        }
    }

    private Vector3 direction;

    // Update is called once per frame
    void FixedUpdate()
    {

        //jos pelaaja sattuu vaihtamaan asetuksiaan kesken pelin gyroskoopin käytön suhteen
        //otetaan gyro pois/päälle
        if (acceToggle != PlayerPrefs.GetInt("acce"))
        {

            acceToggle = PlayerPrefs.GetInt("acce");
            if (PlayerPrefs.GetInt("acce") == 1)
            {
                Input.gyro.enabled = true;
            }
            else
            {
                Input.gyro.enabled = false;
            }
        }

        //jos noppa on pysähtynyt mutta sen sivut eivät kosketa pohjaa (noppa on esim sivuttain reunaa tai toista noppaa vasten)
        //tyhjennetään yläreunan preview-noppanäkymä ja heitetään noppaa uudestaan sattumanvaraisella suunnalla
        if (pysahtyi && !onkoPohja)
        {
            GameObject[] previewnNopat = GameObject.FindGameObjectsWithTag("preview");
            foreach (GameObject preview in previewnNopat)
            {
                Destroy(preview.gameObject);
            }
            silmalasku.GetComponent<silmaluvunLasku>().previewValmis = false;
            silmalasku.GetComponent<silmaluvunLasku>().pysahtyneet = false;
            silmalasku.GetComponent<silmaluvunLasku>().laskettu = false;

            int rX = Random.Range(-5, 5);
            int rY = Random.Range(-5, 5);
            HeitaRandom(rX * 10, rY * 10);
        }

        //noppaa saa heittää uudestaa 0.1s välein, muutoin noppa käyttäytyy hiukan kummallisesti
        //yrittäessä heittää sormen pyyhkäisyllä. Joten aina heiton jälkeen saaHeittää menee falseksi
        //ja 0.1s jälkeen taas trueksi
        if (!saaHeittaa)
        {
            ajastin += Time.deltaTime / 1.5f;

            if (ajastin > 0.1f)
            {
                saaHeittaa = true;
                ajastin = 0;
            }
        }

        //jos näytöllä tunnistetaan 2 sormea, noppia heitetään paikallaan ilman suuntaa
        //ns. "pikaheitto"
        if (Input.touchCount == 2)
            HeitaRandom(0, 0);

        //jos taas sormia on yksi, 
        if (Input.touchCount == 1)
        {
            //otetaan sormelta nopeus ja heitetän sen mukaan (jos se on tarpeeksi suuri)
            foreach (Touch kosketus in Input.touches)
            {
                //deltaPosition antaa 2-ulotteisen vektorin tämän ja edellisen päivityksen välisestä erosta ja magnitude antaa vektorin pituuden
                //eli jos kahden peräkkäisen päivityksen välissä sormen liikevektorin pituus on suurempi kuin 20, heitetään noppaa liikkeen suuntaan
                if (saaHeittaa && kosketus.deltaPosition.magnitude > 20f) HeitaRandom(kosketus.deltaPosition.x, kosketus.deltaPosition.y);
            }
        }

        //tässä tämä kauhea rykelmä nopan pitämiseksi rajojen sisällä...
        if (transform.position.x > posX) transform.position = new Vector3(posX, transform.position.y, transform.position.z);
        if (transform.position.x < -posX) transform.position = new Vector3(-posX, transform.position.y, transform.position.z);
        if (transform.position.z > posZ) transform.position = new Vector3(transform.position.x, transform.position.y, posZ);
        if (transform.position.z < -posZ) transform.position = new Vector3(transform.position.x, transform.position.y, -posZ);

        //napataan nopan nopeuden arvo (velocity antaa 3-ulotteisen nopeusvektorin ja magnitude taas muuntaa sen pituudeksi)
        speed = GetComponent<Rigidbody>().velocity.magnitude;

        //jos olemme vielä pelin viimeisen tiedon mukaan liikkeellä, mutta nopeus on hyvin pieni
        //alamme laskea aikaa olemmeko oikeasti pysähtyneet vai vain kiihtyvässä liikkeessä
        if (liikkeessa && speed < 0.5)
        {
            time += Time.deltaTime / 1.5f;

            //jos satumme pääsemään yli puolen sekunnin, voimme suht turvallisesti sanoa että noppa ei enää tule liikkumaan itsekseen
            if (time > 0.5f)
            {
                pysahtyi = true;
                liikkeessa = false;
            }
        }
        //jos taas nopan nopeus on edellistä arvoa suurempi, nollaamme varmuuden vuoksi ajastimen (jos se vaikka olisi alkanut tikittää
        //muutaman framen verran), ja vaihdamme nopan tilat kohdalleen
        if (speed > 0.5)
        {
            time = 0f;
            pysahtyi = false;
            liikkeessa = true;
        }

        //Jos saamme tarpeeksi suuria arvoja puhelimen gyrosta/kiihdytysmittarista, heitämme noppaa sen mukaan
        if (saaHeittaa && (Input.gyro.userAcceleration.x > acc || Input.gyro.userAcceleration.x < -acc)) HeitaX(Input.gyro.userAcceleration.x, Input.gyro.userAcceleration.y);
        if (saaHeittaa && (Input.gyro.userAcceleration.y > acc || Input.gyro.userAcceleration.y < -acc)) HeitaY(Input.gyro.userAcceleration.y, Input.gyro.userAcceleration.x);
        if (saaHeittaa && Input.gyro.userAcceleration.z > acc) HeitaZ(Input.gyro.userAcceleration.z);

    }

    /// <summary>
    /// heitarandom on hiukan harhaanjohtava, mutta tänne tullaan kun noppaa heitetään sormella pyyhkäisten, yhtä nopppaa yksittäin heittäen,
    /// tai "pikapyöräytyksellä" kahdella sormella painamalla.
    /// </summary>
    /// <param name="x">heiton koordinaatti suuntaan X</param>
    /// <param name="y">heiton koordinaatti suuntaan Y (oikeasi siis 3D maailmassa tämä on Z akseli, mutta tämä oli helpompi siihen aikaan ymmärtää x,y koordinaateilla)</param>
    void HeitaRandom(float x, float y)
    {
        //nostetaan noppaa heitoilla aina hiukan ylös pöydän pinnalta, jotta se mahtuu pyörimään
        //noppa ei kuitenkaan saa nousta liian korkealle, joten kun nopan korkeus on 5.5f, pysäytämme
        //nopan kohoamisen muuntamalla siihen vaikuttavan vektorin nollaan
        float korkeusVektori = 0.2f;
        if (transform.position.y > 5.5f) korkeusVektori = 0f;

        //nopan suunta heitossa
        direction = new Vector3(x / 50, korkeusVektori, y / 50) * 10;

        //asetetaan nopan fysikaaliseen objektiin sen vauhti, pyörimisnopeus
        //ja vielä sattumanvarainen kierto, jotta nopanheitto on 100% varmasti sattumanvarainen aina
        GetComponent<Rigidbody>().velocity = direction;
        GetComponent<Rigidbody>().angularVelocity = laskeAngularVelo();
        GetComponent<Rigidbody>().rotation = Random.rotation;

        //vaihdetaan heiton jälkeiset arvot sen mukaan.
        //emme ole enää pohjassa
        onkoPohja = false;
        //tämä ei ollut yksittäinen heitto
        singleHeitto = false;
        //noppaa on heitetty, eikä se ole jäädytetty (tätä muuttujaa käytetään toisessa skriptissä)
        heittoFreezenJalkeen = true;
        //noppa heitettiin joten ihan heti emme voi vielä heittää uudestaan
        saaHeittaa = false;
    }

    /// <summary>
    /// heitto X:n suuntaan, jos olemme pelilaudan sisällä (käytetään vain gyron aiheuttamissa heitoissa)
    /// </summary>
    /// <param name="vauhtix">gyron vauhti X:n suuntaan</param>
    /// <param name="vauhtiz">gyron vauhti Z:n suuntaan</param>
    void HeitaX(float vauhtix, float vauhtiz)
    {

        //tehdään liikelaskut vain jos olemme pelilaudan sisällä
        if (transform.position.x < posX && transform.position.x > -posX)
        {
            direction = new Vector3(vauhtix, 0, vauhtiz) * 10;
            GetComponent<Rigidbody>().velocity = direction;
        }

        GetComponent<Rigidbody>().angularVelocity = laskeAngularVelo();
        GetComponent<Rigidbody>().rotation = Random.rotation;

        //vaihdetaan heiton jälkeiset arvot sen mukaan.
        //emme ole enää pohjassa
        onkoPohja = false;
        //tämä ei ollut yksittäinen heitto
        singleHeitto = false;
        //noppaa on heitetty, eikä se ole jäädytetty (tätä muuttujaa käytetään toisessa skriptissä)
        heittoFreezenJalkeen = true;
        //noppa heitettiin joten ihan heti emme voi vielä heittää uudestaan
        saaHeittaa = false;
    }

    /// <summary>
    /// Nopan heitto Y:n suuntaan, jos olemme pelilaudan sisällä (käytetään vain gyron aiheuttamissa heitoissa)
    /// 3D maailmassa tämä suunta on siis Z, mutta kun peliä katsotaan ylhäältä päin, tuntui helpommalta ajatella vain x,y koordinaatteina
    /// </summary>
    /// <param name="vauhtiz">gyron vauhti Z:n suuntaan</param>
    /// <param name="vauhtix">gyron vauhti X:n suuntaan</param>
    void HeitaY(float vauhtiz, float vauhtix)
    {

        //tehdään liikelaskut vain jos olemme pelilaudan sisällä
        if (transform.position.z < posZ && transform.position.z > -posZ)
        {
            direction = new Vector3(vauhtix, 0, vauhtiz) * 10;
            GetComponent<Rigidbody>().velocity = direction;
        }

        GetComponent<Rigidbody>().angularVelocity = laskeAngularVelo();
        GetComponent<Rigidbody>().rotation = Random.rotation;

        //vaihdetaan heiton jälkeiset arvot sen mukaan.
        //emme ole enää pohjassa
        onkoPohja = false;
        //tämä ei ollut yksittäinen heitto
        singleHeitto = false;
        //noppaa on heitetty, eikä se ole jäädytetty (tätä muuttujaa käytetään toisessa skriptissä)
        heittoFreezenJalkeen = true;
        //noppa heitettiin joten ihan heti emme voi vielä heittää uudestaan
        saaHeittaa = false;
    }

    /// <summary>
    /// Nopan heitto Z:n suuntaan, jos olemme pelilaudan sisällä (käytetään vain gyron aiheuttamissa heitoissa)
    /// kuten edellä mainitsin, 3D maailmassa tämä on Y akseli
    /// </summary>
    /// <param name="vauhti">gyron vauhti Y:n suuntaan</param>
    void HeitaZ(float vauhti)
    {
        //jos nopan korkeus on liian korkea (pelilaudan "katto" emme laske liikettä)
        if (transform.position.y < 5.5f)
        {
            direction = new Vector3(0, vauhti * 1.5f, 0) * 5;
            GetComponent<Rigidbody>().velocity = direction;
        }
        GetComponent<Rigidbody>().angularVelocity = laskeAngularVelo();
        GetComponent<Rigidbody>().rotation = Random.rotation;

        //vaihdetaan heiton jälkeiset arvot sen mukaan.
        //emme ole enää pohjassa
        onkoPohja = false;
        //tämä ei ollut yksittäinen heitto
        singleHeitto = false;
        //noppaa on heitetty, eikä se ole jäädytetty (tätä muuttujaa käytetään toisessa skriptissä)
        heittoFreezenJalkeen = true;
        //noppa heitettiin joten ihan heti emme voi vielä heittää uudestaan
        saaHeittaa = false;

    }

    /// <summary>
    /// funktio laskemaan nopalle sattumanvarainen pyörimisnopeus heiton yhteyteen
    /// </summary>
    /// <returns>3-ulotteinen vektori, joka ilmaisee kiertonopeuden</returns>
    Vector3 laskeAngularVelo()
    {
        return new Vector3(Random.Range(-angularVelo, angularVelo), Random.Range(-angularVelo, angularVelo), Random.Range(-angularVelo, angularVelo)) * 10000;
    }

    /// <summary>
    /// Unityn oma funktio, johon mennään jos fysiikka-objektit huomaavat törmäävänsä johonkin
    /// </summary>
    /// <param name="collision">törmäys, joka sisältää mm. objektin, johon törmättiin</param>
    void OnCollisionEnter(Collision collision)
    {

        //haetaan taas asetuksista nopan heiton äänitila, jos pelaaja olisikin muuttanut sitä kesken pelin
        throwTila = PlayerPrefs.GetInt("throw", 1);

        //jos tila on 1, soitamme ääniä
        if (throwTila == 1)
        {
            //jos törmäyskohde on pelikentän seinä tai pohja, soitamme jotakin ääniefektiä
            //emme huomioi törmäyksiä muihin noppiin
            if (collision.gameObject.tag == "pohja" || collision.gameObject.tag == "seina")
            {
                //jos meillä on käytössä kolikko, käytämme vain kolikon ääniä
                //kolikosta on vain "lyhyet" äänet olemassa.
                if (Regex.IsMatch(GetComponent<MeshFilter>().mesh.name, "^Coin"))
                {
                    //jos mitään ääntä ei kuulu, voimme soittaa ääniefektin
                    //muutoin ääniefektit saattaisivat soida useaan kertaan peräkkäin
                    if (!sound.isPlaying)
                    {
                        //valitaan sattumanvarainen heittoääni ja soitetaan se
                        //äänen voimakkuuteen vaikuttaa törmäyksen nopeus
                        int rnd = Random.Range(0, shorts.Length);
                        GetComponent<AudioSource>().clip = shorts[rnd] as AudioClip;
                        sound.volume = collision.relativeVelocity.magnitude / 20;
                        sound.Play();
                    }
                }

                //jos objektimme ei ole kolikko, se on sitten noppa, jolloin voimme soittaa noppaääniä
                //nopilla on pidempiä ja lyhyempiä ääniefektejä, joita soitamme riippuen törmäyksen nopeudesta/suunnasta
                else
                {
                    //jos noppa tippuu suoraan ylhäältä pienellä nopeudella, soitamme "shorts" kansiosta lyhyempiä
                    //heittoääniä, jotta saavuttaisimme hiukan realistisempaa äänimaailmaa
                    if (collision.relativeVelocity.magnitude < 10f || (collision.relativeVelocity.x == 0 && collision.relativeVelocity.z == 0))
                    {
                        //jos mitään ääntä ei kuulu, voimme soittaa ääniefektin
                        //muutoin ääniefektit saattaisivat soida useaan kertaan peräkkäin
                        if (!sound.isPlaying)
                        {
                            //valitaan sattumanvarainen heittoääni ja soitetaan se
                            //äänen voimakkuuteen vaikuttaa törmäyksen nopeus
                            int rnd = Random.Range(0, shorts.Length);
                            GetComponent<AudioSource>().clip = shorts[rnd] as AudioClip;
                            sound.volume = collision.relativeVelocity.magnitude / 20;
                            sound.Play();
                        }
                    }

                    //jos nopalla on tarpeeksi nopea vauhti törmäyksessä, valitsemme "longs" kansiosta ääniä
                    if (collision.relativeVelocity.magnitude >= 10f)
                    {
                        //jos mitään ääntä ei kuulu, voimme soittaa ääniefektin
                        //muutoin ääniefektit saattaisivat soida useaan kertaan peräkkäin
                        if (!sound.isPlaying)
                        {
                            //valitaan sattumanvarainen heittoääni ja soitetaan se
                            //äänen voimakkuuteen vaikuttaa törmäyksen nopeus
                            int rnd = Random.Range(0, longs.Length);
                            GetComponent<AudioSource>().clip = longs[rnd] as AudioClip;
                            sound.volume = collision.relativeVelocity.magnitude / 20;
                            sound.Play();
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Unityn oma funktio jota kutsutaan kun fysiikka-objektit "erkanevat" toisistaan
    /// </summary>
    /// <param name="collision">törmäys, joka sisältää mm. objektin, johon törmättiin</param>
    void OnCollisionExit(Collision collision)
    {
        //jos objekti, josta irtoamme on pohja, vaihdamme pohjakosketuksen muuttujan sen mukaan
        if (collision.gameObject.tag == "pohja")
            onkoPohja = false;
    }
}
